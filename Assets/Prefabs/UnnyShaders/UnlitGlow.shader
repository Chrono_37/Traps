﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

    Shader "UnnyWorld/Unlit with glow" {
        Properties {
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _GlowTex ("Glow", 2D) = "white" {}
            _GlowColor ("Glow Color", Color) = (1,1,1,1)
        }
        SubShader {
            Tags { "RenderType"="Opaque" }
            LOD 200
           
            Pass {
                Tags { "LightMode" = "Always" }
               
                ZWrite On
                Lighting Off
       
                CGPROGRAM
                	//#pragma exclude_renderers xbox360
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma fragmentoption ARB_precision_hint_fastest

                    uniform sampler2D _MainTex;
                    uniform sampler2D _GlowTex;
                    fixed4 _GlowColor;
                    uniform float4 _MainTex_ST;

                    struct appdata {
                        float4 vertex : POSITION;
                        float4 texcoord : TEXCOORD0;
                    };
                   
                    struct v2f {
                        float4 vertex : POSITION;
                        float2 texcoord : TEXCOORD0;
                    };
                   
                    v2f vert (appdata v) {
                        v2f o;
                        o.texcoord = _MainTex_ST.xy * v.texcoord + _MainTex_ST.zw;
                        o.vertex = UnityObjectToClipPos(v.vertex);
                        return o;
                    }
                   
                    fixed4 frag (v2f i) : COLOR {
                        half4 texcol = tex2D (_MainTex, i.texcoord.xy);
                        half4 glow = tex2D (_GlowTex, i.texcoord.xy);
                        return texcol + _GlowColor * glow.x;
                    }
                ENDCG
       
            }
        }
    }
     
