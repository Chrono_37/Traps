﻿Shader "UnnyWorld/Particles/Additive" {
    Properties {
    _MainTex ("Particle Texture", 2D) = "white" {}
    _Color( "Color", Color) = (1,1,1,0.5)
}
 
Category {
    Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
    Blend SrcAlpha One
    Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
   
    BindChannels {
        Bind "Vertex", vertex
        Bind "TexCoord", texcoord
    }
   
    SubShader {
        Pass {
            SetTexture [_MainTex] {
            	constantColor [_Color]
                combine constant, constant * texture
            }
        }
    }
}
}


