﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UnnyWorld/Color With Mask" {
	Properties {
		_MainTex ("Texture (RGB)", 2D) = "white" {}
        _MaskTex ("Mask ", 2D) = "black" {}
        _MainColor ("Main Color", Color) = (1,1,1,1)
        _Scale ("Scale", Float) = 1
        // required for UI.Mask
	}
   
    SubShader {
        Tags { "QUEUE"="Background+100" "IGNOREPROJECTOR"="true" "RenderType"="AlphaTest" "PreviewType"="Plane"}
         Pass {

         Lighting Off

CGPROGRAM
// Upgrade NOTE: excluded shader from Xbox360; has structs without semantics (struct v2f members bumpuv)
//#pragma exclude_renderers xbox360

#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

struct appdata {
    float4 vertex : POSITION;
    float2 texcoord : TEXCOORD;
};

struct v2f {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD0;
};

uniform float4 _MainTex_ST;
v2f vert(appdata v)
{
    v2f o;
    o.pos = UnityObjectToClipPos (v.vertex);
    o.uv =  v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return o;
}

uniform sampler2D _MainTex;
uniform sampler2D _MaskTex;
uniform float4 _MainColor;
uniform float _Scale;

half4 frag( v2f i ) : COLOR
{
    half4 texcol = tex2D (_MainTex, i.uv);
    half k = tex2D (_MaskTex, i.uv).r * _Scale;
    texcol = _MainColor * k + texcol * max(0,1-k*10);
    return texcol;
}
ENDCG
        }
    }
Fallback "VertexLit"
} 
