﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UnnyWorld/Colors By Mask" {
	Properties {
		_MainTex ("Mask (RGB)", 2D) = "white" {}
        _LightTex ("Lightning ", 2D) = "white" {}
        _MainColor ("Main Color", Color) = (1,1,1,1)
        _ColorR ("Red Color", Color) = (1,1,1,1)
        _ColorG ("Green Color", Color) = (1,1,1,1)
        _ColorB ("Blue Color", Color) = (1,1,1,1)
        // required for UI.Mask
	}
   
    SubShader {
        Tags { "QUEUE"="Background+100" "IGNOREPROJECTOR"="true" "RenderType"="AlphaTest" "PreviewType"="Plane"}
         Pass {

         Lighting Off

CGPROGRAM
// Upgrade NOTE: excluded shader from Xbox360; has structs without semantics (struct v2f members bumpuv)
//#pragma exclude_renderers xbox360

#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

struct appdata {
    float4 vertex : POSITION;
    float2 texcoord : TEXCOORD;
};

struct v2f {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD0;
};

uniform float4 _MainTex_ST;
v2f vert(appdata v)
{
    v2f o;
    o.pos = UnityObjectToClipPos (v.vertex);
    o.uv =  v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return o;
}

uniform sampler2D _MainTex;
uniform sampler2D _LightTex;
uniform float4 _ColorR, _ColorG, _ColorB, _MainColor;

half4 frag( v2f i ) : COLOR
{
    half4 texcol = tex2D (_MainTex, i.uv);  
    texcol = texcol.r * _ColorR + texcol.g * _ColorG + texcol.b * _ColorB;
    texcol = _MainColor * max(0, (1-texcol.r-texcol.g-texcol.b)) + texcol;
    texcol *= tex2D (_LightTex, i.uv);
    return texcol;
}
ENDCG
        }
    }
Fallback "VertexLit"
} 
