// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UnnyWorld/Particles/Alpha Blended Scale" {
Properties {
    _ScaleColor ("Scale Color", Float) = 1
    _MainTex ("Particle Texture", 2D) = "white" {}
}

Category {
    Tags { "Queue"="Transparent +100" "IgnoreProjector"="True" "RenderType"="Transparent" }
    Blend SrcAlpha OneMinusSrcAlpha
    ColorMask RGB
    Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
    BindChannels {
        Bind "Color", color
        Bind "Vertex", vertex
        Bind "TexCoord", texcoord
    }

// ---- Fragment program cards
SubShader {
Pass {

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#pragma fragmentoption ARB_fog_exp2

#include "UnityCG.cginc"

sampler2D _MainTex;
float _ScaleColor;

struct appdata_t {
    float4 vertex : POSITION;
    float4 color : COLOR;
    float2 texcoord : TEXCOORD0;
};

struct v2f {
    float4 vertex : POSITION;
    float4 color : COLOR;
    float2 texcoord : TEXCOORD0;
};

float4 _MainTex_ST;

v2f vert (appdata_t v)
{
    v2f o;
    o.vertex = UnityObjectToClipPos(v.vertex);
    o.color = v.color;
    o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
    return o;
}

half4 frag (v2f i) : COLOR
{
    float4 c = i.color * tex2D(_MainTex, i.texcoord);
    c.xyz = c.xyz * _ScaleColor;
    return c;
}
ENDCG
}
}

// ---- Dual texture cards
SubShader {
Pass {
    SetTexture [_MainTex] {
        constantColor [_TintColor]
        combine constant * primary
    }
    SetTexture [_MainTex] {
        combine texture * previous DOUBLE
    }
    }
}

// ---- Single texture cards (does not do color tint)
SubShader {
    Pass {
        SetTexture [_MainTex] {
            combine texture * primary
        }
    }
}
}
}