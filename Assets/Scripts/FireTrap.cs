﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrap : Trap
{
    void Start ()
    {
        Start_GetComponents();
        Start_CheckDirection();
        Start_CheckPower();
    }

    void Update()
    {
        Update_Trap();
    }

    protected override void Update_ActivateTrap()
    {
        if (m_AffectedCellCount != 0)
            m_Animator.SetBool("IsActivated", true);
        SetParticleEffects();
        switch (m_Direction)
        {
            case "right":
                {
                    if (m_MoveBlockTransform.position.x < m_Transform.position.x + m_AffectedCellCount)
                        m_MoveBlockTransform.position += new Vector3(Time.deltaTime * MoveSpeed, 0, 0);
                    break;
                }
            case "left":
                {
                    if (m_MoveBlockTransform.position.x > m_Transform.position.x - m_AffectedCellCount)
                        m_MoveBlockTransform.position -= new Vector3(Time.deltaTime * MoveSpeed, 0, 0);
                    break;
                }
            case "up":
                {
                    if (m_MoveBlockTransform.position.z < m_Transform.position.z + m_AffectedCellCount)
                        m_MoveBlockTransform.position += new Vector3(0, 0, Time.deltaTime * MoveSpeed);
                    break;
                }
            case "down":
                {
                    if (m_MoveBlockTransform.position.z > m_Transform.position.z - m_AffectedCellCount)
                        m_MoveBlockTransform.position -= new Vector3(0, 0, Time.deltaTime * MoveSpeed);
                    break;
                }
            default:
                break;
        }
    }

    void SetParticleEffects()
    {
        ParticleSystem.MainModule main = m_ParticleEffect.main;
        main.startLifetime = 0.1f * m_AffectedCellCount;
        m_ParticleEffect.Play();
    }
}
