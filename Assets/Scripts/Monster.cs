﻿using System.Collections;
using UnityEngine;

public class Monster : Humanoid
{
    const float RotationSpeed = 10;

    public string m_MonsterType;
    Transform[] m_TrappersTransforms;
    int m_TargetedTrapperNumber;

    void Start ()
    {
        Start_GetComponents();
        m_TrappersTransforms = m_MovementController.m_TrapperCellTransforms;
    }
	
	void Update ()
    {
        Update_LookAtPlayer();
    }

    void Update_LookAtPlayer()
    {
        float minDistance = 100;
        for (int i = 0; i < m_TrappersTransforms.Length; i++)
        {
            if (m_TrappersTransforms[i] != null)
            {
                float distance = Vector3.Distance(m_TrappersTransforms[i].position, m_Transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    m_TargetedTrapperNumber = i;
                }
            }
        }
        if (m_TrappersTransforms[m_TargetedTrapperNumber] != null)
        {
            m_Transform.rotation = Quaternion.Slerp(m_Transform.rotation, Quaternion.LookRotation
                (m_TrappersTransforms[m_TargetedTrapperNumber].position - m_Transform.position), 
                Time.deltaTime * RotationSpeed);
        }
        Vector3 rotation = m_Transform.rotation.eulerAngles;
        m_Transform.rotation = Quaternion.Euler(0, rotation.y, rotation.z);
    }

    public IEnumerator HuntForTrapper()
    {
        if (GameManager.singleton.IsMonstersHuntingTrapsAppearingState())
        {
            m_Animator.SetBool("attack", true);
            yield return new WaitForSeconds(0.2f);
            m_MovementController.HitTrapper(m_TrappersTransforms[m_TargetedTrapperNumber]);
            yield return new WaitForSeconds(0.4f);
            m_Animator.SetBool("attack", false);
        }
    }
}
