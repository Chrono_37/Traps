﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    const int WinScore = 5;
    const int RequiredWavesForExtraLife = 3;

    public static GameManager singleton;
    [SerializeField]
    GameObject m_PauseButton, m_PausePanel, m_ResumeButton, m_NextLevelButton;
    [SerializeField]
    Text m_ScoreText;
    MovementController m_MovementController;
    internal bool m_AreLivesAdded;
    internal int m_Score, m_RowCount;
    internal int m_TrappersCount, m_MonstersCount, m_AliveTrappersCount, m_AliveMonstersCount;

    public enum GameState
    {
        Movement,
        TrapsWorking,
        MonstersHuntingTrapsAppearing,
        GameWon,
        GameLost
    }
    public GameState m_GameState = GameState.Movement;

    void Awake()
    {
        singleton = this;
    }

    void Start()
    {
        m_MovementController = MovementController.singleton;
        Start_GetSceneComponents();
    }

    void Update()
    {
        m_ScoreText.text = "Score: " + m_Score.ToString();
        if (m_Score % RequiredWavesForExtraLife == 0 & m_Score != 0)
        {
            if (!m_AreLivesAdded)
            {
                m_AreLivesAdded = true;
                AddLives();
            }
        }
        if (m_Score == WinScore)
        {
            m_GameState = GameState.GameWon;
            m_PausePanel.SetActive(true);
            m_NextLevelButton.SetActive(true);
        }
    }

    void Start_GetSceneComponents()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName.Contains("4x4"))
            m_RowCount = 4;
        else if (sceneName.Contains("4x5"))
            m_RowCount = 5;
        else if (sceneName.Contains("4x6"))
            m_RowCount = 6;
    }

    public bool IsMovementState()
    {
        return m_GameState == GameState.Movement;
    }

    public bool IsTrapsWorkingState()
    {
        return m_GameState == GameState.TrapsWorking;
    }

    public bool IsMonstersHuntingTrapsAppearingState()
    {
        return m_GameState == GameState.MonstersHuntingTrapsAppearing;
    }

    public bool IsGameWonState()
    {
        return m_GameState == GameState.GameWon;
    }

    public bool IsGameLostState()
    {
        return m_GameState == GameState.GameLost;
    }

    void AddLives()
    {
        for (int i = 0; i < m_TrappersCount; i++)
        {
            if (m_MovementController.m_Trappers[i] != null)
                m_MovementController.m_Trappers[i].GetComponent<Trapper>().m_Lives++;
        }
    }

    public IEnumerator MakeMonstersHunt()
    {
        for (int i = 0; i < m_MonstersCount; i++)
        {
            Monster monster;
            if (m_MovementController.m_Monsters[i] != null)
            {
                monster = m_MovementController.m_Monsters[i].GetComponent<Monster>();
                yield return new WaitForSeconds(1f);
                StartCoroutine(monster.HuntForTrapper());
            }
        }
    }

    public void AllTrappersDead()
    {
        m_GameState = GameState.GameLost;
        m_PausePanel.SetActive(true);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        Resume();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Pause()
    {
        Time.timeScale = 0;
        m_PausePanel.SetActive(true);
        m_ResumeButton.SetActive(true);
        m_PauseButton.SetActive(false);
        MakeTrapsVisible(false);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        m_PauseButton.SetActive(true);
        MakeTrapsVisible(true);
        m_PausePanel.SetActive(false);
        m_ResumeButton.SetActive(false);
        m_NextLevelButton.SetActive(false);
    }

    public void NextLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    void MakeTrapsVisible(bool isEnable)
    {
        List<GameObject> trapsObjects = TrapsController.singleton.m_AllTrapsObjects;
        if (trapsObjects.Count != 0)
        {
            for (int i = 0; i < trapsObjects.Count; i++)
            {
                trapsObjects[i].SetActive(isEnable);
            }
        }
    }
}
