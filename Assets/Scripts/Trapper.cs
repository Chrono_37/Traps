﻿using UnityEngine;

public class Trapper : Humanoid
{
    const int MaxLives = 3;

    [SerializeField]
    TextMesh m_LivesText;
    public int m_Lives;

    void Start ()
    {
        m_Lives = MaxLives;
        Start_GetComponents();
    }

    void Update ()
    {
        m_LivesText.text = m_Lives.ToString();
    }
}
