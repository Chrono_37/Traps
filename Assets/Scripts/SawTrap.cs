﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawTrap : Trap
{
    [SerializeField]
    GameObject m_WarningObject;
    Vector3 m_StartPosition;

    void Start()
    {
        Start_GetComponents();
        m_StartPosition = m_Transform.position;
    }

    void Update()
    {
        Update_Trap();
    }

    protected override void Update_ActivateTrap()
    {
        m_WarningObject.SetActive(false);
        m_Transform.position = m_StartPosition + new Vector3(0, 0.7f, 0);
    }
}
