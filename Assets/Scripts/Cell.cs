﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    internal bool m_IsAffected;
    Transform m_Transform;
    //Renderer m_Renderer;
    Color m_DefaultColor;
    MovementController m_MovementController;
    float m_OnStayTimer;
    GameManager m_GM;

    void Start()
    {
        m_Transform = transform;
        //m_Renderer = GetComponent<Renderer>();
        m_MovementController = MovementController.singleton;
        m_GM = GameManager.singleton;
        //m_DefaultColor = m_Renderer.material.color;
    }

    void Update()
    {
        if (m_IsAffected)
        {
            if (m_GM.IsMonstersHuntingTrapsAppearingState())
                m_IsAffected = false;
        }
        //DefaultColor();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "TrapElectro":
                {
                    TrapsController.singleton.InstantiateElectroTrapParticles(m_Transform.position);
                    m_IsAffected = true;
                    m_MovementController.TrapOnCellAction("Electro", m_Transform);
                    break;
                }
            case "TrapFire":
                {
                    m_IsAffected = true;
                    m_MovementController.TrapOnCellAction("Fire", m_Transform);
                    break;
                }
            case "TrapSaw":
                {
                    m_IsAffected = true;
                    m_MovementController.TrapOnCellAction("Saw", m_Transform);
                    break;
                }
            case "Player":
                {
                    m_MovementController.HumanoidOnCellAction(true, m_Transform, other.name);
                    //m_Renderer.material.color = Color.blue;
                    break;
                }
            case "Monster":
                {
                    m_MovementController.HumanoidOnCellAction(false, m_Transform, other.name);
                    //m_Renderer.material.color = Color.green;
                    break;
                }
            default:
                break;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!m_GM.IsTrapsWorkingState())
        {
            if (other.CompareTag("Player"))
            {
                m_MovementController.HumanoidOnCellAction(true, m_Transform, other.name);
                //m_Renderer.material.color = Color.blue;
            }
            if (other.CompareTag("Monster"))
            {
                m_MovementController.HumanoidOnCellAction(false, m_Transform, other.name);
                //m_Renderer.material.color = Color.green;
            }
        }
    }

    //void DefaultColor()
    //{
    //    if (!m_MovementController.IsHumanoidOnCellCheck(m_Transform) & !m_IsAffected)
    //        m_Renderer.material.color = m_DefaultColor;
    //}

    void OnMouseDown()
    {
        if (m_MovementController.m_ActiveHumanoid != null)
        {
            if (m_GM.IsMovementState() & !m_MovementController.IsHumanoidOnCellCheck(m_Transform))
            {
                m_MovementController.DisableOutline();
                m_MovementController.m_ActiveHumanoid.transform.position
                    = m_Transform.position + new Vector3(0, 0.25f, 0);
                m_MovementController.m_ActiveHumanoid = null;
            }
        }
    }
}
