﻿using cakeslice;
using UnityEngine;

public class Humanoid : MonoBehaviour
{
    internal Animator m_Animator;
    protected MovementController m_MovementController;
    protected Camera m_Camera;
    protected Transform m_Transform;
    protected Vector3 HumanoidYPosition, m_TouchPosition;
    protected Outline[] m_OulineObjects;
    protected float m_CameraYHeight;

    protected void Start_GetComponents()
    {
        m_Camera = Camera.main;
        m_Transform = transform;
        m_CameraYHeight = m_Camera.transform.position.y;
        m_OulineObjects = GetComponentsInChildren<Outline>();
        m_MovementController = MovementController.singleton;
        m_Animator = GetComponentInChildren<Animator>();
        HumanoidYPosition = new Vector3(0, m_Transform.position.y, 0);
    }

    void OnMouseDown()
    {
        if (GameManager.singleton.IsMovementState())
        {
            if (m_MovementController.m_ActiveHumanoid != null)
            {
                m_MovementController.DisableOutline();
                Vector3 temp = m_Transform.position;
                m_Transform.position = m_MovementController.m_ActiveHumanoid.transform.position;
                m_MovementController.m_ActiveHumanoid.transform.position = temp;
                m_MovementController.m_ActiveHumanoid = null;
            }
            else
            {
                for (int i = 0; i < m_OulineObjects.Length; i++)
                {
                    m_OulineObjects[i].enabled = true;
                }
                m_MovementController.m_ActiveHumanoid = gameObject;
            }
        }
    }
}
