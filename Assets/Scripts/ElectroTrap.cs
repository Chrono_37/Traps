﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroTrap : Trap
{
    Vector3 m_MoveBlockStartPosition;

    void Start()
    {
        Start_GetComponents();
        Start_CheckDirection();
        Start_CheckPower();
        m_MoveBlockStartPosition = m_MoveBlockTransform.position;
    }

    void Update()
    {
        Update_Trap();
    }

    protected override void Update_ActivateTrap()
    {
        if (m_AffectedCellCount != m_MaxCellsCount)
            m_Animator.SetBool("IsActivated", true);
        switch (m_Direction)
        {
            case "right":
                {
                    if (m_MoveBlockStartPosition == m_MoveBlockTransform.position)
                        m_MoveBlockTransform.position = m_MoveBlockTransform.position + 
                            new Vector3(m_AffectedCellCount + 1, 0, 0);
                    if (m_MoveBlockTransform.position.x < m_Transform.position.x + 10)
                        m_MoveBlockTransform.position += new Vector3(Time.deltaTime * MoveSpeed, 0, 0);
                    break;
                }
            case "left":
                {
                    if (m_MoveBlockStartPosition == m_MoveBlockTransform.position)
                        m_MoveBlockTransform.position = m_MoveBlockTransform.position -
                            new Vector3(m_AffectedCellCount + 1, 0, 0);
                    if (m_MoveBlockTransform.position.x > m_Transform.position.x - 10)
                        m_MoveBlockTransform.position -= new Vector3(Time.deltaTime * MoveSpeed, 0, 0);
                    break;
                }
            case "up":
                {
                    if (m_MoveBlockStartPosition == m_MoveBlockTransform.position)
                        m_MoveBlockTransform.position = m_MoveBlockTransform.position +
                            new Vector3(0, 0, m_AffectedCellCount + 1);
                    if (m_MoveBlockTransform.position.z < m_Transform.position.z + 10)
                        m_MoveBlockTransform.position += new Vector3(0, 0, Time.deltaTime * MoveSpeed);
                    break;
                }
            case "down":
                {
                    if (m_MoveBlockStartPosition == m_MoveBlockTransform.position)
                        m_MoveBlockTransform.position = m_MoveBlockTransform.position -
                            new Vector3(0, 0, m_AffectedCellCount + 1);
                    if (m_MoveBlockTransform.position.z > m_Transform.position.z - 10)
                        m_MoveBlockTransform.position -= new Vector3(0, 0, Time.deltaTime * MoveSpeed);
                    break;
                }
            default:
                break;
        }
    }
}
