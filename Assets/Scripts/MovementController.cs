﻿using cakeslice;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    const float TrapsWorkingTime = 2;

    public static MovementController singleton;
    public GameObject[] m_Trappers;
    public GameObject[] m_Monsters;
    internal Transform[] m_TrapperCellTransforms, m_MonsterCellTransforms;
    internal GameObject m_ActiveHumanoid;
    GameManager m_GM;
    Trapper[] m_TrappersComponents;

    void Awake()
    {
        singleton = this;
    }

    void Start()
    {
        m_GM = GameManager.singleton;
        m_GM.m_TrappersCount = m_Trappers.Length;
        m_TrappersComponents = new Trapper[m_Trappers.Length];
        m_GM.m_MonstersCount = m_Monsters.Length;
        m_GM.m_AliveTrappersCount = m_Trappers.Length;
        m_GM.m_AliveMonstersCount = m_Monsters.Length;
        m_TrapperCellTransforms = new Transform[m_Trappers.Length];
        m_MonsterCellTransforms = new Transform[m_Monsters.Length];
        for (int i = 0; i < m_Trappers.Length; i++)
        {
            m_TrappersComponents[i] = m_Trappers[i].GetComponent<Trapper>();
        }
    }

    public bool IsHumanoidOnCellCheck(Transform cellTransform)
    {
        if (m_TrapperCellTransforms.Contains(cellTransform) |
            m_MonsterCellTransforms.Contains(cellTransform))
            return true;
        else
            return false;
    }

    public void TrapOnCellAction(string trapType, Transform cellTransform)
    {
        if (m_TrapperCellTransforms.Contains(cellTransform))
            HitTrapper(cellTransform);
        else if (m_MonsterCellTransforms.Contains(cellTransform))
        {
            int monsterCellIndex = Array.IndexOf(m_MonsterCellTransforms, cellTransform);
            Monster hitedMonster = m_Monsters[monsterCellIndex].GetComponent<Monster>();
            if (hitedMonster.m_MonsterType == trapType | hitedMonster.m_MonsterType == "Usual")
            {
                m_GM.m_AliveMonstersCount--;
                m_MonsterCellTransforms[monsterCellIndex] = null;
                StartCoroutine(KillHumanoid(hitedMonster, monsterCellIndex, false));
            }
        }
    }

    IEnumerator KillHumanoid(Humanoid humanoid, int humanoidCellIndex, bool isTrapper)
    {
        humanoid.m_Animator.SetBool("dead", true);
        yield return new WaitForSeconds(TrapsWorkingTime * 0.4f);
        if (isTrapper)
        {
            m_TrapperCellTransforms[humanoidCellIndex] = null;
            m_TrappersComponents[humanoidCellIndex] = null;
            Destroy(m_Trappers[humanoidCellIndex]);
        }
        else
            Destroy(m_Monsters[humanoidCellIndex]);
    }

    IEnumerator HitTrapperAnimation(Trapper hitedTrapper)
    {
        hitedTrapper.m_Animator.SetBool("hitted", true);
        yield return new WaitForSeconds(0.2f);
        hitedTrapper.m_Animator.SetBool("hitted", false);
    }

    public void SetTrapperAnimatorBool(string command, bool isTrue)
    {
        for (int i = 0; i < m_TrappersComponents.Length; i++)
        {
            if (m_TrappersComponents[i] != null)
                m_TrappersComponents[i].m_Animator.SetBool(command, isTrue);
        }
    }

    public void HitTrapper(Transform cellTransform)
    {
        int trapperCellIndex = Array.IndexOf(m_TrapperCellTransforms, cellTransform);
        Trapper hitedTrapper;
        if (m_Trappers[trapperCellIndex] != null)
        {
            hitedTrapper = m_Trappers[trapperCellIndex].GetComponent<Trapper>();
            StartCoroutine(HitTrapperAnimation(hitedTrapper));
            hitedTrapper.m_Lives--;
            if (hitedTrapper.m_Lives == 0)
            {
                m_GM.m_AliveTrappersCount--;
                StartCoroutine(KillHumanoid(hitedTrapper, trapperCellIndex, true));
                if (m_GM.m_AliveTrappersCount <= 0)
                    GameManager.singleton.AllTrappersDead();
            }
        }
    }

    public void HumanoidOnCellAction(bool isTrapper, Transform cellTransform, string humanoidName)
    {
        int humanoidNumber = Convert.ToInt32(humanoidName.Substring(7)) - 1;
        if (isTrapper)
            m_TrapperCellTransforms[humanoidNumber] = cellTransform;
        else
            m_MonsterCellTransforms[humanoidNumber] = cellTransform;
    }

    public void DisableOutline()
    {
        Outline[] outlineObjects = m_ActiveHumanoid.GetComponentsInChildren<Outline>();

        for (int i = 0; i < outlineObjects.Length; i++)
        {
            outlineObjects[i].enabled = false;
        }
    }
}
