﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    const float MonsterYPosition = 0.05f;

    internal static MonsterSpawner singleton;
    internal GameObject[] m_Cells;
    [SerializeField]
    Transform m_MonstersParentObject;
    [SerializeField]
    GameObject[] m_MonsterPrefabs;
    GameManager m_GM;
    MovementController m_MovementController;

    void Awake()
    {
        singleton = this;
    }

	void Start ()
    {
        m_GM = GameManager.singleton;
        m_MovementController = MovementController.singleton;
        m_Cells = GameObject.FindGameObjectsWithTag("Cell");
    }
	
    public void SpawnMonster()
    {
        if (m_GM.IsMonstersHuntingTrapsAppearingState())
        {
            int randomCell = Random.Range(0, m_Cells.Length);
            int randomMonster = Random.Range(0, m_MonsterPrefabs.Length - 1);
            GameObject monster;
            Transform cellTransform = m_Cells[randomCell].transform;
            if (!m_MovementController.IsHumanoidOnCellCheck(cellTransform))
            {
                monster = Instantiate(m_MonsterPrefabs[randomMonster],
                    cellTransform.position + new Vector3(0, MonsterYPosition, 0), Quaternion.identity);
                monster.transform.SetParent(m_MonstersParentObject);
                m_GM.m_AliveMonstersCount++;
                for (int i = 0; i < m_MovementController.m_Monsters.Length; i++)
                {
                    if (m_MovementController.m_Monsters[i] == null)
                    {
                        monster.name = "Monster" + (i + 1);
                        m_MovementController.m_Monsters[i] = monster;
                        i = 10;
                    }
                }
            }
            else SpawnMonster();
        }
    }
}
