﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public const float MoveSpeed = 8;
    const int ColumnCount = 4;

    [SerializeField]
    protected GameObject m_ParticleObject, m_AffectedCellsNumberObject;
    [SerializeField]
    protected Transform m_MoveBlockTransform;
    protected Animator m_Animator;
    protected TrapsController m_TrapsController;
    protected Transform m_Transform;
    protected ParticleSystem m_ParticleEffect;
    protected TextMesh m_AffectedCellsText;
    protected string m_Direction;
    protected int m_AffectedCellCount, m_MaxCellsCount;

    protected void Start_GetComponents()
    {
        m_TrapsController = TrapsController.singleton;
        m_Animator = GetComponentInChildren<Animator>();
        m_Transform = transform;
    }

    protected void Start_CheckDirection()
    {
        int rowCount = m_TrapsController.m_RowCount;
        if (m_Transform.position.x < 0)
        {
            m_Transform.rotation = Quaternion.Euler(0, 90, 0);
            m_Direction = "right";
            m_MaxCellsCount = ColumnCount;
        }
        else if (m_Transform.position.x > ColumnCount)
        {
            m_Transform.rotation = Quaternion.Euler(0, -90, 0);
            m_Direction = "left";
            m_MaxCellsCount = ColumnCount;
        }
        else if (m_Transform.position.z < 0)
        {
            m_Direction = "up";
            m_MaxCellsCount = rowCount;
        }
        else if (m_Transform.position.z > rowCount)
        {
            m_Transform.rotation = Quaternion.Euler(0, 180, 0);
            m_Direction = "down";
            m_MaxCellsCount = rowCount;
        }
    }

    protected void Start_CheckPower()
    {
        m_AffectedCellCount = Random.Range(0, m_MaxCellsCount + 1);
        m_AffectedCellsNumberObject.transform.rotation = Quaternion.Euler(90, 0, 0);
        if (m_ParticleObject != null)
            m_ParticleEffect = m_ParticleObject.GetComponent<ParticleSystem>();
        m_AffectedCellsText = m_AffectedCellsNumberObject.GetComponent<TextMesh>();
        m_AffectedCellsText.text = m_AffectedCellCount.ToString();
    }

    protected void Update_Trap()
    {
        if (GameManager.singleton.IsTrapsWorkingState())
            Update_ActivateTrap();
    }

    protected virtual void Update_ActivateTrap()
    {

    }
}
