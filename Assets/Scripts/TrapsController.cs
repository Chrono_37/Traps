﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapsController : MonoBehaviour
{
#pragma warning disable 0649

    const int ColumnCount = 4;
    const float TrapsAppearTime = 0.5f;
    const float TrapsWorkingTime = 2;
    const float MaxThinkingTime = 3;
    const float MonstersHuntingTime = 2;
    const float CellLengthHalf = 0.5f;
    const float TrapsYHeight = 0.3f;

    public static TrapsController singleton;
    [SerializeField]
    float m_ThinkingTime;
    [SerializeField, Range(0, 5)]
    int m_FireTrapCount;
    [SerializeField, Range(0, 5)]
    int m_ElectroTrapCount;
    [SerializeField, Range(0, 9)]
    int m_SawTrapCount;
    [SerializeField]
    GameObject m_ComingTrapPrefab, m_FireTrapPrefab, m_ElectroTrapPrefab, m_SawTrapPrefab;
    MovementController m_MovementController;
    public GameObject m_ElectroTrapParticles;
    [SerializeField]
    Material m_CellMaterial;
    internal List<GameObject> m_AllTrapsObjects = new List<GameObject>();
    internal int m_RowCount;
    int FireTrapsPositionsCount, ElectroTrapsPositionsCount, SawTrapsPositionsCount;
    GameManager m_GM;
    Vector3[] m_FireTrapsPositions, m_ElectroTrapsPositions, m_SawTrapsPositions;
    List<int> m_TrapsSettledPositions = new List<int>();
    float m_GameTimer, m_WaveTime;
    bool m_AreAllTrapsSet;

    void Awake()
    {
        singleton = this;
    }

    void Start()
    {
        m_GM = GameManager.singleton;
        m_MovementController = MovementController.singleton;
        m_CellMaterial.SetFloat("_Scale", 0.85f);
        Start_CountTrapsPositions();
        Start_SetFireTrapsPositions();
        Start_SetElectroTrapsPositions();
        Start_SetSawTrapsPositions();
        StartCoroutine(SetTraps("FireTrap"));
    }

    void Update()
    {
        m_GameTimer += Time.deltaTime;
        Update_CountThinkingCoef();
        if (m_AreAllTrapsSet & m_GM.IsMovementState())
        {
            if (m_GameTimer - m_ThinkingTime > m_WaveTime)
            {
                m_AreAllTrapsSet = false;
                m_WaveTime = m_GameTimer;
                StartCoroutine(ActivateTraps());
            }
        }
    }

    void Start_CountTrapsPositions()
    {
        m_RowCount = m_GM.m_RowCount;
        FireTrapsPositionsCount = 2 * (m_RowCount + ColumnCount);
        ElectroTrapsPositionsCount = FireTrapsPositionsCount;
        SawTrapsPositionsCount = m_RowCount * ColumnCount;
        m_FireTrapsPositions = new Vector3[FireTrapsPositionsCount];
        m_ElectroTrapsPositions = new Vector3[ElectroTrapsPositionsCount];
        m_SawTrapsPositions = new Vector3[SawTrapsPositionsCount];
    }

    void Start_SetFireTrapsPositions()
    {
        int j = 0;
        for (int i = 0; i < m_RowCount; i++)
        {
            m_FireTrapsPositions[i] = new Vector3(-CellLengthHalf, TrapsYHeight,
                CellLengthHalf + j);
            j++;
        }
        j = 0;
        for (int i = m_RowCount; i < m_RowCount + ColumnCount; i++)
        {
            m_FireTrapsPositions[i] = new Vector3(CellLengthHalf + j, TrapsYHeight,
                -CellLengthHalf);
            j++;
        }
        j = 0;
        for (int i = m_RowCount + ColumnCount; i < 2 * m_RowCount + ColumnCount; i++)
        {
            m_FireTrapsPositions[i] = new Vector3(CellLengthHalf + ColumnCount, TrapsYHeight,
                CellLengthHalf + j);
            j++;
        }
        j = 0;
        for (int i = 2 * m_RowCount + ColumnCount; i < 2 * (m_RowCount + ColumnCount); i++)
        {
            m_FireTrapsPositions[i] = new Vector3(CellLengthHalf + j, TrapsYHeight,
                CellLengthHalf + m_RowCount);
            j++;
        }
    }

    void Start_SetElectroTrapsPositions()
    {
        m_ElectroTrapsPositions = m_FireTrapsPositions;
    }

    void Start_SetSawTrapsPositions()
    {
        GameObject[] cells = MonsterSpawner.singleton.m_Cells;
        for (int i = 0; i < cells.Length; i++)
        {
            m_SawTrapsPositions[i] = cells[i].transform.position + new Vector3(0, -CellLengthHalf, 0);
        }
    }

    void Update_CountThinkingCoef()
    {
        int thinkingCoef = m_GM.m_AliveTrappersCount;
        m_ThinkingTime = thinkingCoef * m_ThinkingTime;
        if (m_ThinkingTime > MaxThinkingTime)
            m_ThinkingTime = MaxThinkingTime;
    }

    IEnumerator SetTraps(string trapType)
    {
        if (m_GM.IsMovementState())
        {
            GameObject trapPrefab = null;
            Vector3[] trapsPositions = null;
            int trapsCount = 0;
            int TrapPositionsCount = 0;
            switch (trapType)
            {
                case "FireTrap":
                    {
                        trapPrefab = m_FireTrapPrefab;
                        trapsPositions = m_FireTrapsPositions;
                        trapsCount = m_FireTrapCount;
                        TrapPositionsCount = FireTrapsPositionsCount;
                        StartCoroutine(SetTraps("SawTrap"));
                        break;
                    }
                case "SawTrap":
                    {
                        if (m_SawTrapCount != 0)
                            yield return new WaitForSeconds(m_FireTrapCount - 2);
                        trapPrefab = m_SawTrapPrefab;
                        trapsPositions = m_SawTrapsPositions;
                        trapsCount = m_SawTrapCount;
                        TrapPositionsCount = SawTrapsPositionsCount;
                        StartCoroutine(SetTraps("ElectroTrap"));
                        break;
                    }
                case "ElectroTrap":
                    {
                        if (m_ElectroTrapCount != 0)
                            yield return new WaitForSeconds(m_SawTrapCount - 2);
                        trapPrefab = m_ElectroTrapPrefab;
                        trapsPositions = m_ElectroTrapsPositions;
                        trapsCount = m_ElectroTrapCount;
                        TrapPositionsCount = ElectroTrapsPositionsCount;
                        m_WaveTime = m_GameTimer;
                        m_AreAllTrapsSet = true;
                        break;
                    }
                default:
                    break;
            }
            for (int i = 0; i < trapsCount; i++)
            {
                int trapPosition = Random.Range(0, TrapPositionsCount);
                int checkRepeatsCount = 100;
                for (int j = 0; j < checkRepeatsCount; j++)
                {
                    if (m_TrapsSettledPositions.Contains(trapPosition))
                        trapPosition = Random.Range(0, TrapPositionsCount);
                    else
                    {
                        m_TrapsSettledPositions.Add(trapPosition);
                        checkRepeatsCount = j;
                    }
                }
                GameObject comingBox = Instantiate(m_ComingTrapPrefab, trapsPositions[trapPosition],
                    Quaternion.Euler(0, 180, 0));
                m_AllTrapsObjects.Add(comingBox);
                yield return new WaitForSeconds(TrapsAppearTime);
                m_AllTrapsObjects.Remove(comingBox);
                Destroy(comingBox);
                GameObject trap = Instantiate(trapPrefab, trapsPositions[trapPosition],
                    Quaternion.identity);
                m_AllTrapsObjects.Add(trap);
            }
        }
    }

    public void InstantiateElectroTrapParticles(Vector3 cellPosition)
    {
        GameObject electroParticle = Instantiate(m_ElectroTrapParticles, cellPosition
            + new Vector3(0, 1, 0), Quaternion.identity);
        m_AllTrapsObjects.Add(electroParticle);
    }

    IEnumerator ActivateTraps()
    {
        m_MovementController.SetTrapperAnimatorBool("crouch", true);
        m_CellMaterial.SetFloat("_Scale", 0.65f);
        yield return new WaitForSeconds(TrapsWorkingTime / 2);
        m_GM.m_GameState = GameManager.GameState.TrapsWorking;
        if (m_MovementController.m_ActiveHumanoid != null)
        {
            m_MovementController.DisableOutline();
            m_MovementController.m_ActiveHumanoid = null;
        }
        yield return new WaitForSeconds(TrapsWorkingTime);
        if (m_GM.IsTrapsWorkingState())
        {
            m_GM.m_GameState = GameManager.GameState.MonstersHuntingTrapsAppearing;
            for (int i = 0; i < m_AllTrapsObjects.Count; i++)
            {
                Destroy(m_AllTrapsObjects[i]);
            }
            m_AllTrapsObjects.Clear();
            m_TrapsSettledPositions.Clear();
            int aliveMonstersCount = m_GM.m_AliveMonstersCount;
            int maxMonstersCount = m_GM.m_MonstersCount;
            if (aliveMonstersCount != 0)
                StartCoroutine(m_GM.MakeMonstersHunt());
            yield return new WaitForSeconds(TrapsWorkingTime / 3 + aliveMonstersCount * 1.5f);
            m_GM.m_Score++;
            if (m_GM.m_AreLivesAdded)
                m_GM.m_AreLivesAdded = false;
            m_MovementController.SetTrapperAnimatorBool("crouch", false);
            if (aliveMonstersCount < maxMonstersCount & maxMonstersCount != 0)
            {
                int spawnCount = Random.Range(1, maxMonstersCount - aliveMonstersCount + 1);
                for (int i = 1; i < spawnCount + 1; i++)
                {
                    MonsterSpawner.singleton.SpawnMonster();
                }
            }
            m_CellMaterial.SetFloat("_Scale", 0.85f);
            m_GM.m_GameState = GameManager.GameState.Movement;
            StartCoroutine(SetTraps("FireTrap"));
        }
    }
}
