﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject m_MainMenu, m_LevelSelector;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void Play()
    {
        SceneManager.LoadSceneAsync("4x4_Easy");
    }

    public void EnterSelectLevelMenu()
    {
        m_MainMenu.SetActive(false);
        m_LevelSelector.SetActive(true);
    }

    public void EnterStoreMenu()
    {

    }

    public void EnterSettingsMenu()
    {

    }

    public void Back()
    {
        m_LevelSelector.SetActive(false);
        m_MainMenu.SetActive(true);
    }
}
